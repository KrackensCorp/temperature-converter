# frozen_string_literal: true

require_relative '../lib/converter_ckf'
require 'rspec'

RSpec.describe ConverterCKF do
  describe '.converter' do
    context 'Из C в F' do
      it { expect(ConverterCKF.convert(42, 'C', 'F')).to eq 107 }
    end

    context 'Из C в K' do
      it { expect(ConverterCKF.convert(42, 'C', 'K')).to eq 315 }
    end

    context 'Из F в C' do
      it { expect(ConverterCKF.convert(42, 'F', 'C')).to eq(5) }
    end

    context 'Из F в K' do
      it { expect(ConverterCKF.convert(42, 'F', 'K')).to eq 297 }
    end

    context 'Из K в C' do
      it { expect(ConverterCKF.convert(42, 'K', 'C')).to eq(-231) }
    end

    context 'Из K в F' do
      it { expect(ConverterCKF.convert(42, 'K', 'F')).to eq(-384) }
    end
  end
end
