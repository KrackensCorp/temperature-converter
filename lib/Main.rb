# frozen_string_literal: true

require_relative 'converter_ckf'

class Main
  def self.number_reader
    numbers = gets.chomp
    if numbers.to_f.to_i.to_s == numbers
      numbers.to_i
    else
      'error'
    end
  end

  def self.start
    puts 'Введите значение в градусах:'
    value = number_reader
    if value == 'error'
      puts 'Некорректное значение, программа завершена.'
    else
      puts 'Введите еденицу измерения данных градусов: (C, F, K)'
      from = gets.chomp
      puts 'Введите еденицу измерения для конвертации: (C, F, K)'
      to = gets.chomp
      puts 'Результат конвертации:', ConverterCKF.convert(value, from, to)
    end
  end
end

Main.start
