# frozen_string_literal: true

class ConverterCKF

  def self.convert(value, from, to)
    case from
    when 'C', 'c'
      convert_from_c(value, to)
    when 'K', 'k'
      convert_from_k(value, to)
    when 'F', 'f'
      convert_from_f(value, to)
    end
  end

  def self.convert_from_c(value, to)
    case to
    when 'F', 'f'
      (value * 9 / 5) + 32
    when 'K', 'k'
      value + 273
    end
  end

  def self.convert_from_k(value, to)
    case to
    when 'C', 'c'
      value - 273
    when 'F', 'f'
      (value * 9 / 5) - 459
    end
  end

  def self.convert_from_f(value, to)
    case to
    when 'C', 'c'
      (value - 32) * 5 / 9
    when 'K', 'k'
      value + 459 * 5 / 9
    end
  end
end
